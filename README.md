
## Requirements

1. Php 7.1.5.
2. Apache web server.
3. MySQL 5.6.


## How to start

1. Clone repository to your Web Server directory.
2. Migrate db using db_dump.sql.
3. You may need to adjust db settings in app/base/model.php .
4. Enjoy!

