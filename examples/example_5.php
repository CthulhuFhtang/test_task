<?php

/* "==" - при сравнение приводит к одному типу, а "===" - нет
*/
$foo = 5;
$bar = "5";

if ($foo == $bar)
	echo "Even first";
if ($foo === $bar)
	echo "Even second";