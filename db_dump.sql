CREATE DATABASE test_task;
USE test_task;
CREATE TABLE `bids` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_event` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `price` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
INSERT INTO `bids` (`id`, `id_event`, `name`, `email`, `price`) VALUES
(1, 1, 'Василий', 'vas@gmai.com', 100),
(2, 1, 'Николай', 'nk@gmail.com', 150);

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `caption` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `events` (`id`, `caption`) VALUES
(1, 'Atlas Weekend 2017'),
(2, 'Green Gray');


CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `task_desc` varchar(500) COLLATE utf8_bin NOT NULL,
  `task_code` varchar(10000) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


INSERT INTO `tasks` (`id`, `task_desc`, `task_code`) VALUES
(0, '1) Написать алгоритм решения задачи:\r\nВ классе 28 учеников. 75% из них занимаются спортом. Сколько учеников в классе\r\nзанимаются спортом и сколько всего учеников в классе?\r\n', '$pupils = 28;\r\n\r\necho \"Занимаются спортом = \" . ($pupils * 75) / 100 . \"<br> Всего = \" . $pupils; '),
(1, '2) Реализовать алгоритм извлечения числовых значений со строки:\r\nThis server has 386 GB RAM and 5000 GB storage\r\n', '$string =  \"This server has 386 GB RAM and 5000 GB storage\";\r\n\r\n$string_pieces = explode (\" \", $string);\r\n$result = array();\r\nforeach ($string_pieces as $pice) \r\n	if (is_numeric($pice))\r\n		array_push($result, $pice);\r\n\r\nprint_r($result);'),
(2, '3) ​Как получить первый элемент массива ​[2,3,56,65,56,44,34,45,3,5,35,345,3,5] ​?\r\n', '$arr = array(2,3,56,65,56,44,34,45,3,5,35,345,3,5);\r\n\r\necho $arr[0];'),
(3, '4) Как вычислить остаток от деления 10/3', 'echo 10%3;'),
(4, '5) Нужно поменять 2 переменные местами без использования третьей:<pre><code>$а = [1,2,3,4,5];\r\n$b = ‘Hello world’;</code></pre>', '$foo = [1,2,3,4,5];\r\n$bar = \'Hello world\'; \r\n\r\nlist($foo, $bar) = array($bar, $foo);\r\n\r\nprint_r(\"foo = \" . $foo . \"<br> bar = \");\r\nprint_r($bar);\r\n'),
(5, '6) Чем отличается оператор == от === ?', '/* \"==\" - при сравнение приводит к одному типу, а \"===\" - нет\r\n*/\r\n$foo = 5;\r\n$bar = \"5\";\r\n\r\nif ($foo == $bar)\r\n	echo \"Even first\";\r\nif ($foo === $bar)\r\n	echo \"Even second\";'),
(6, '7) Чем отличается require от include ?', '/* require - обязательное включение, include - нет\r\n*/\r\n\r\ninclude \"apps/there_is_no_such_file.php\";\r\nrequire \"apps/there_is_no_such_file.php\";'),
(7, '8) Какие данные пользователя сайта из перечисленных можно считать на 100%\r\nдостоверными: cookie, данные сессии, IP-адрес пользователя , User-Agent? Почему?', '/*\r\nДанные сессии - ибо только они хранятся на сервере\r\n*/'),
(8, '9) Что выведет следующий код на JavaScript и почему:<pre><code>for( var i =0; i < 10; i++){\r\nsetTimeout(function () {\r\nconsole.log(i);\r\n}, 100);\r\n}</code></pre>', 'Выведет 10 - ибо setTimeout выпадает из жизненного цикла js'),
(9, '10. Сделать скрипты для подготовки базы данных(миграции)', '\r\nCREATE TABLE `bids` (\r\n  `id` int(10) UNSIGNED NOT NULL,\r\n  `id_event` int(10) UNSIGNED NOT NULL,\r\n  `name` varchar(50) COLLATE utf8_bin DEFAULT NULL,\r\n  `email` varchar(50) COLLATE utf8_bin DEFAULT NULL,\r\n  `price` float DEFAULT NULL\r\n) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;\r\n\r\nINSERT INTO `bids` (`id`, `id_event`, `name`, `email`, `price`) VALUES\r\n(1, 1, \'Василий\', \'vas@gmai.com\', 100),\r\n(2, 1, \'Николай\', \'nk@gmail.com\', 150);\r\n\r\n\r\nCREATE TABLE `events` (\r\n  `id` int(10) UNSIGNED NOT NULL,\r\n  `caption` varchar(100) COLLATE utf8_bin DEFAULT NULL\r\n) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;\r\n\r\nINSERT INTO `events` (`id`, `caption`) VALUES\r\n(1, \'Atlas Weekend 2017\'),\r\n(2, \'Green Gray\');\r\n\r\n\r\nCREATE TABLE `tasks` (\r\n  `id` int(10) UNSIGNED NOT NULL,\r\n  `task_desc` varchar(500) COLLATE utf8_bin NOT NULL,\r\n  `task_code` varchar(500) COLLATE utf8_bin NOT NULL\r\n) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;\r\n\r\n\r\nINSERT INTO `tasks` (`id`, `task_desc`, `task_code`) VALUES\r\n(0, \'1) Написать алгоритм решения задачи:\\r\\nВ классе 28 учеников. 75% из них занимаются спортом. Сколько учеников в классе\\r\\nзанимаются спортом и сколько всего учеников в классе?\\r\\n\', \'$pupils = 28;\\r\\n\\r\\necho \\\"Занимаются спортом = \\\" . ($pupils * 75) / 100 . \\\"<br> Всего = \\\" . $pupils; \'),\r\n(1, \'2) Реализовать алгоритм извлечения числовых значений со строки:\\r\\nThis server has 386 GB RAM and 5000 GB storage\\r\\n\', \'$string =  \\\"This server has 386 GB RAM and 5000 GB storage\\\";\\r\\n\\r\\n$string_pieces = explode (\\\" \\\", $string);\\r\\n$result = array();\\r\\nforeach ($string_pieces as $pice) \\r\\n	if (is_numeric($pice))\\r\\n		array_push($result, $pice);\\r\\n\\r\\nprint_r($result);\'),\r\n(2, \'3) ​Как получить первый элемент массива ​[2,3,56,65,56,44,34,45,3,5,35,345,3,5] ​?\\r\\n\', \'$arr = array(2,3,56,65,56,44,34,45,3,5,35,345,3,5);\\r\\n\\r\\necho $arr[0];\'),\r\n(3, \'4) Как вычислить остаток от деления 10/3\', \'echo 10%3;\'),\r\n(4, \'5) Нужно поменять 2 переменные местами без использования третьей:$а = [1,2,3,4,5];\\r\\n$b = ‘Hello world’;\', \'$foo = [1,2,3,4,5];\\r\\n$bar = \\\'Hello world\\\'; \\r\\n\\r\\nlist($foo, $bar) = array($bar, $foo);\\r\\n\\r\\nprint_r(\\\"foo = \\\" . $foo . \\\"<br> bar = \\\");\\r\\nprint_r($bar);\\r\\n\'),\r\n(5, \'6) Чем отличается оператор == от === ?\', \'/* \\\"==\\\" - при сравнение приводит к одному типу, а \\\"===\\\" - нет\\r\\n*/\\r\\n$foo = 5;\\r\\n$bar = \\\"5\\\";\\r\\n\\r\\nif ($foo == $bar)\\r\\n	echo \\\"Even first\\\";\\r\\nif ($foo === $bar)\\r\\n	echo \\\"Even second\\\";\'),\r\n(6, \'7) Чем отличается require от include ?\', \'/* require - обязательное включение, include - нет\\r\\n*/\\r\\n\\r\\ninclude \\\"apps/there_is_no_such_file.php\\\";\\r\\nrequire \\\"apps/there_is_no_such_file.php\\\";\'),\r\n(7, \'8) Какие данные пользователя сайта из перечисленных можно считать на 100%\\r\\nдостоверными: cookie, данные сессии, IP-адрес пользователя , User-Agent? Почему?\', \'/*\\r\\nДанные сессии - ибо только они хранятся на сервере\\r\\n*/\'),\r\n(8, \'9) Что выведет следующий код на JavaScript и почему:for( var i =0; i < 10; i++){\\r\\nsetTimeout(function () {\\r\\nconsole.log(i);\\r\\n}, 100);\\r\\n}\', \'somecode\'),\r\n(9, \'10. Сделать скрипты для подготовки базы данных(миграции)\', \'somecode\'),\r\n(10, \'11. Напишите запрос, который выберет имя пользователя (bids.name) с самой\\r\\nвысокой ценой заявки (bids.price)\\r\\n\', \'somecode\'),\r\n(11, \'12. Напишите запрос, который выберет название мероприятия (events.caption), по\\r\\nкоторому нет заявок\\r\\n\', \'somecode\'),\r\n(12, \'13. Напишите запрос, который выберет название мероприятия (events.caption), по\\r\\nкоторому больше трех заявок\', \'somecode\'),\r\n(13, \'14. Напишите запрос, который выберет название мероприятия (events.caption), по\\r\\nкоторому больше всего заявок\', \'somecode\');\r\n\r\nALTER TABLE `bids`\r\n  ADD PRIMARY KEY (`id`),\r\n  ADD KEY `id_event` (`id_event`);\r\n\r\nALTER TABLE `events`\r\n  ADD PRIMARY KEY (`id`);\r\n\r\nALTER TABLE `tasks`\r\n  ADD UNIQUE KEY `id` (`id`);\r\n\r\nALTER TABLE `bids`\r\n  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;\r\n\r\nALTER TABLE `bids`\r\n  ADD CONSTRAINT `bids_ibfk_1` FOREIGN KEY (`id_event`) REFERENCES `events` (`id`);\r\nCOMMIT;\r\n\r\n'),
(10, '11. Напишите запрос, который выберет имя пользователя (bids.name) с самой\r\nвысокой ценой заявки (bids.price)\r\n', 'return $db->query(\"SELECT name FROM bids ORDER BY price DESC LIMIT 1\");'),
(11, '12. Напишите запрос, который выберет название мероприятия (events.caption), по\r\nкоторому нет заявок\r\n', 'return $db->query(\"SELECT caption FROM events WHERE id NOT IN (SELECT id_event FROM bids)\");'),
(12, '13. Напишите запрос, который выберет название мероприятия (events.caption), по\r\nкоторому больше трех заявок', 'return $db->query(\"SELECT caption FROM events INNER JOIN bids on events.id = bids.id_event GROUP BY id_event HAVING COUNT(id_event) > 3 \");'),
(13, '14. Напишите запрос, который выберет название мероприятия (events.caption), по\r\nкоторому больше всего заявок', 'return $db->query(\"SELECT caption FROM events INNER JOIN bids on events.id = bids.id_event GROUP BY id_event HAVING MAX(id_event)\");');

ALTER TABLE `bids`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_event` (`id_event`);

ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tasks`
  ADD UNIQUE KEY `id` (`id`);

ALTER TABLE `bids`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `bids`
  ADD CONSTRAINT `bids_ibfk_1` FOREIGN KEY (`id_event`) REFERENCES `events` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
