<?php

/**
* Base class for all views
*/
class View 
{
	function display ($content, $template, $data = null)
	{
		include 'app/views/'. $template;
	}
}