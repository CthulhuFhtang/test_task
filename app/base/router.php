<?php  

/**
* Class used to get to appropriate conroller, model and method
*/
class Router
{
	//list of url routes
	private $urls;

	function __construct()
	{
		$url_path = 'urls.php';
		$this->urls = include($url_path);
	}

	//get current url
	private function get_url()
	{
		if (!empty($_SERVER['REQUEST_URI']))
			return trim($_SERVER['REQUEST_URI'], '/');
	}
	public function start()
	{
		$controller = 'main';
		$action = 'index';
		$url = $this->get_url();

		//search by pattern in url
		foreach ($this->urls as $url_pattern => $path) 
		{
			if (preg_match("~$url_pattern~", $url))
			{
				$route = preg_replace("~$url_pattern~", $path, $url);
							
				$elements = explode('/', $route);
				$controller = array_shift($elements);
				$action = array_shift($elements);
				$variables = array_shift($elements);

				$controller_name = 'Controller_' . ucfirst($controller);
				$controller_file = $controller_name .'.php';
				$model_file = 'Model_'. ucfirst($controller). '.php';
				$action_name = 'action_' .$action;

				if (file_exists('app/controllers/' .$controller_file))
				{
					include('app/controllers/' .$controller_file);
				}
				else 
				{
					header( 'Location: /');
				}


				if (file_exists('app/models/' .$model_file))
				{
					include('app/models/' .$model_file);
				}


				$curr_controller = new $controller_name;

				if (method_exists($curr_controller, $action_name))
				{
					call_user_func_array(array($curr_controller, $action_name), array($variables));
				}

				die;

			}

		}



		//some shit going here

	}
}

