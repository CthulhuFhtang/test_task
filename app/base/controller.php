<?php

/**
* Base class for all controllers
*/
class Controller 
{
	public $view;
	public $model;

	function __construct()
	{
		$this->view = new View();
	}

	function action_index ($variables)
	{
		//some default action
	}
}