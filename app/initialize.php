<?php

require_once 'base/model.php';
require_once 'base/view.php';
require_once 'base/controller.php';

require_once 'base/router.php';

$router = new Router();
$router->start();