<!DOCTYPE html>
<?php session_start()?>
<html lang="ru">
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css ">
	<link rel="stylesheet" type="text/css" href="../../css/css/font-awesome.min.css">

	<link rel="stylesheet" type="text/css" href="../../css/main.css">

<title>Test task</title>
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top headroom" >
		<div class="container">
			<div class="navbar-header">
				<div class = "navbar-brand">Задачник</div>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav pull-right">
					<li><a href="/">Главная</a></li>
				</ul>
			</div>
		</div>
	</div> 



	<?php include 'app/views/' .$content ?>

	<footer id="footer">
	<div class = "navbar-fixed-bottom row-fluid">
		<div class="footer2">
			<div class="container">
				<div class="row">

					<div class="col-md-6 widget">
						<div class="widget-body">
							<p class="text-right">
								Test task 2018 
							</p>
						</div>
					</div>

				</div> 
			</div>
		</div>
	</div>
</body>
</html>