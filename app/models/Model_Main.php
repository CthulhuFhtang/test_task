<?php

/**
* 
*/
class Model_Main extends Model
{
	
	function get_data()
	{
		$db = $this->connect();
		return $db->query('SELECT * FROM problems');
	}

	function get_sorted_data($field)
	{
		$db = $this->connect();
		return $db->query('SELECT * FROM problems ORDER BY '. $field);
	}
	function get_tasks()
	{
		$db = $this->connect();
		return $db->query('SELECT id, task_desc FROM tasks');
	}
}