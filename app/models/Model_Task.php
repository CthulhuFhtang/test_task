<?php

/*
*Model for task View
*/

class Model_task extends Model
{
	function get_one_task($tasknum)
	{
		$db = $this->connect();
		return $db->query("SELECT * FROM tasks WHERE id =". $tasknum);
	}
	function example_10()
	{
		$db = $this->connect();
		return $db->query("SELECT name FROM bids ORDER BY price DESC LIMIT 1");
	}
	function example_11()
	{
		$db = $this->connect();
		return $db->query("SELECT caption FROM events WHERE id NOT IN (SELECT id_event FROM bids)");
	}
	function example_12()
	{
		$db = $this->connect();
		return $db->query("SELECT caption FROM events INNER JOIN bids on events.id = bids.id_event GROUP BY id_event HAVING COUNT(id_event) > 3 ");
	}
	function example_13()
	{
		$db = $this->connect();
		return $db->query("SELECT caption FROM events INNER JOIN bids on events.id = bids.id_event GROUP BY id_event HAVING MAX(id_event)");
	}
}