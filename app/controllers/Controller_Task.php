<?php

/*
*View tasks and answers
*/

class Controller_Task extends Controller
{

	function __construct()
	{
		$this->view = new View();
		$this->model = new Model_Task();
	}

	function action_index($task_num)
	{
		$curr_task = $this->model->get_one_task($task_num);
		$curr_task_data = $curr_task->fetch();
		$this->view->display('view_task.php', 'view_base.php', $curr_task_data);
	}

}