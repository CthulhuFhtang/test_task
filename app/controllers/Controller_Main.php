<?php

/**
* 
*/
class Controller_Main extends Controller
{
	function __construct()
	{
		$this->view = new View();
		$this->model = new Model_Main();
	}
	
	function action_index($page_num)
	{
		$data = $this->model->get_tasks();

		$tasks = array();
		while ($row = $data->fetch(PDO::FETCH_ASSOC))
    		array_push($tasks, $row);

		$variables = array();
		$variables[0] = $tasks;

		$this->view->display('view_main.php','view_base.php',$variables);
	}
}